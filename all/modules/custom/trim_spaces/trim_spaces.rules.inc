<?php
/**
 * Implement hook_rules_action_info()
 * Declare any meta data about actions for Rules
 */
function trim_spaces_rules_action_info() {

  $actions = array(
    'trim_spaces_action' => array(
        'label' => t('trim string: L, R, double spaces'),
        'group' => t('cadena'),

        // data types: https://www.drupal.org/node/905632
        
        // parameter = el array que ingresas
        'parameter' => array(

          'text_to_trim' => array(
                  'type' => 'text',
                  'label' => t('Text to trim'),
                  ),
              ),

        // provides = el array que obtienes
        'provides' => array(
            'text_trimmed' => array(
                    'type' => 'text',
                    'label' => t('text without extra spaces'),
                    'wrap' => TRUE,
                    //'format' => 'full_html'
                    ),
                  ),
      ),
    );
      return $actions;

} // fin de actions_info

// rules function return value
function trim_spaces_action($text_to_trim) {

  // https://stackoverflow.com/questions/1703320/remove-excess-whitespace-from-within-a-string
  $text_trimmed = trim(preg_replace('/\s+/',' ', $text_to_trim));

  //return $text_trimmed
  return array('text_trimmed' => $text_trimmed);

} // fin de funcion rules trim spaces
