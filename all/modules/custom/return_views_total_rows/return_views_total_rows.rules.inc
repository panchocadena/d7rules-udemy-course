<?php
/**
 * Implement hook_rules_action_info()
 * Declare any meta data about actions for Rules
 */
function return_views_total_rows_rules_action_info() {

  $actions = array(

    // rules function count rows
    'return_count_rows_action' => array(
        'label' => t('return_count_rows'),
        'group' => t('cadena'),
        'parameter' => array(

          'view_name_parameter' => array(
            'type' => 'text',
            'label' => t('View Name'),
            ),

          'view_display_id_parameter' => array(
            'type' => 'text',
            'label' => t('View Display ID'),
            ),

          'argument_id' => array(
            'type' => 'integer',
            'label' => t('Views argument ID'),
            ),

        ),

        'provides' => array(
            'return_count_rows' => array(
              'type' => 'integer',
              'label' => t('Total rows'),
              ),
          ),
      ),

    );
      return $actions;
} // fin de actions_info

//drupal_set_message(t('Something @var just happened.', array('@var' => $pleasant)), 'butterflies');


// rules function return count of rows
function return_count_rows_action($view_name_parameter, $view_display_id_parameter, $argument_id) {

  $return_count_rows = array();
  $view_name = $view_name_parameter;
  $display_id = $view_display_id_parameter;

  $view = views_get_view($view_name);
  $view->set_display($display_id);
  $view->set_arguments(array($argument_id));
  $view->pre_execute();
  $view->execute(); // aqui tienes todo el view object
  $return_count_rows = count( $view->result );

  //drupal_set_message('Rows: '. $return_count_rows, 'warning', FALSE);

  return array (
        'return_count_rows' => $return_count_rows,
      );

} //end funcion return_count_polizas_ayer_action
