<?php
/**
 * Implement hook_rules_action_info()
 * Declare any meta data about actions for Rules
 */
function empty_cart_rules_action_info() {

  $actions = array(
    'empty_cart_action' => array(
        'label' => t('Empty cart'),
        'group' => t('cadena'),
      ),
    );
      return $actions;

} // fin de actions_info


// https://gist.github.com/aramboyajyan/3a55ec9f947ae758737c
// rules function
function empty_cart_action() {
  global $user;
  $order = commerce_cart_order_load($user->uid);
  commerce_cart_order_empty($order);
} // fin de funcion rules action
