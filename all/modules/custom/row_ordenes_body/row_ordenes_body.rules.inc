<?php
/**
 * Implement hook_rules_action_info()
 * Declare any meta data about actions for Rules
 */

function row_ordenes_body_rules_action_info() {

  $actions = array(
    'row_ordenes_body_action' => array(
        'label' => t('construye un ROW ORDENES deL email'),
        'group' => t('cadena'),

        'parameter' => array(

          'order_id' => array(
                  'type' => 'integer',
                  'label' => t('order id'),
                  ),

          'cliente' => array(
                  'type' => 'text',
                  'label' => t('Nombre del cliente'),
                  ),

          'tipo_cliente' => array(
                  'type' => 'text',
                  'label' => t('Tipo de cliente'),
                  ),
          'cantidad' => array(
                  'type' => 'integer',
                  'label' => t('Cantidad'),
                  ),

          'monto_total' => array(
                  'type' => 'decimal',
                  'label' => t('Monto total orden'),
                  ),
          'monto_flete' => array(
                  'type' => 'integer',
                  'label' => t('Monto flete'),
                  ),


          'region_text_name' => array(
                  'type' => 'text',
                  'label' => t('Region name'),
                  ),

              ), // fin parameter

        'provides' => array(
            'text_formatted_body' => array(
                    'type' => 'text',
                    'label' => t('text_formatted body'),
                    'wrap' => TRUE,
                    //'format' => 'full_html'
                    ),
                  ), // fin de provides
      ),
    );
      return $actions;

} // fin de actions_info

// rules function procesar row
function row_ordenes_body_action($order_id, $cliente, $tipo_cliente, $cantidad,
    $monto_total, $monto_flete, $region_text_name) {

      //hace el render del row
      $text_formatted_body =
      "
    ▁▁▁▁▁▁ Orden $order_id ▁▁▁▁▁▁▁
    Cliente: $cliente
    Tipo de cliente = $tipo_cliente
    Cantidad total de sacos: $cantidad
    Monto de la orden:  $monto_total
    Monto del flete: $monto_flete
    Región: $region_text_name
      " ;

  //return $text_formatted_body
  return array('text_formatted_body' => $text_formatted_body);



} // fin de funcion rules procesar row
