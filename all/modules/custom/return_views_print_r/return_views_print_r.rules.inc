<?php
/**
 * Implement hook_rules_action_info()
 * Declare any meta data about actions for Rules
 */
function return_views_print_r_rules_action_info() {

  $actions = array(

    // rules function count rows
    'return_print_r_action' => array(
        'label' => t('return_print_r($view)'),
        'group' => t('cadena'),
        'parameter' => array(

          'view_name_parameter' => array(
            'type' => 'text',
            'label' => t('View Name'),
            ),

          'view_display_id_parameter' => array(
            'type' => 'text',
            'label' => t('View Display ID'),
            ),

          'argument_id' => array(
            'type' => 'integer',
            'label' => t('Views argument ID'),
            ),

        ),


      ),

    );
      return $actions;
} // fin de actions_info

//drupal_set_message(t('Something @var just happened.', array('@var' => $pleasant)), 'butterflies');


// rules function return count of rows
function return_print_r_action($view_name_parameter, $view_display_id_parameter, $argument_id) {

  $return_print_r = array();
  $view_name = $view_name_parameter;
  $display_id = $view_display_id_parameter;

  $view = views_get_view($view_name);
  $view->set_display($display_id);
  $view->set_arguments(array($argument_id));
  $view->pre_execute();
  $view->execute(); // aqui tienes todo el view object

  // function construye el print_r from view
  function imprimir_r($view) {
   echo "<pre>";
      print_r($view);
   echo "</pre>";
  }
  // ejecuto el function print_r from the view
  imprimir_r($view);

  // function updates_nodes_from_a_view {
  //   $nid = $node_id // variable from views_get_view
  //   $node = node_load($nid);
  //   // do stuff with node_load
  //   node_save($node);
  // }

} //end funcion return_print_r
