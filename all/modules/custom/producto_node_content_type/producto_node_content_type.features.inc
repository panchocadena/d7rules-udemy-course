<?php

/**
 * @file
 * producto_node_content_type.features.inc
 */

/**
 * Implements hook_node_info().
 */
function producto_node_content_type_node_info() {
  $items = array(
    'producto_node' => array(
      'name' => t('producto node'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
